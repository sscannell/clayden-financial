<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clayden Financial</title>

    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/landing.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/landing-responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  
  <div id="logoBanner" class="row">
  	<div class="col-md-12 alignCenter">
    	<img src="img/landing-logo.jpg" />
    </div>
  </div>
  
  <div id="desktop" class="container">
      <div id="siteChoice" class="row">
      		<div class="col-md-6 col-sm-6">
            	<div class="alignCenter">
                	<a href="business/index.php"><img src="img/landing-business.png" /></a>
                	<h1>Business</h1>
               </div>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing 
               elementum turpis sit amet dictum. Maecenas euismod sit amet risus ut dictum. 
               Sed diam nisl, rhoncus eu arcu sit amet, tempus consectetur libero.</p> 
              <div class="alignCenter doublemart">
                <a href="business/index.php" class="button">Enter Site</a>
              </div>
           </div>
           <div class="col-md-6 col-sm-6">
           	<div class="alignCenter">
                	<a href="personal/index.php"><img src="img/landing-personal.png" /></a>
            		<h1>Personal</h1>
            	</div>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing 
               elementum turpis sit amet dictum. Maecenas euismod sit amet risus ut dictum. 
               Sed diam nisl, rhoncus eu arcu sit amet, tempus consectetur libero.</p> 
               <div class="alignCenter doublemart">
                    <a href="personal/index.php" class="button">Enter Site</a>
               </div>
           </div>	
      </div>
      <div class="row doublemart">
          <div class="landingCopyright col-md-12 alignCenter">
                <p>Copyright © 2013 Clayden Financial. All rights reserved.</p>
          </div>
     </div>
  </div>
  
  
  <div id="mobile" class="container">
      <div id="siteChoice">
      	<div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 alignCenter">
                	<img class="iconChoice" src="img/landing-business.png" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 alignCenter">
                <h1>Business</h1>
                    <a href="business/index.php" class="button">Enter Site</a>
            </div>
      </div>
      <br />
      <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 alignCenter">
                	<img class="iconChoice" src="img/landing-personal.png" />
           </div>
           <div class="col-md-6 col-sm-6 col-xs-6 alignCenter">
                <h1>Personal</h4>
                    <a href="personal/index.php" class="button">Enter Site</a>
            </div>
         </h1>
      </div>
  </div>
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    
    <script>
		$(document).ready(function(){
		  $(".calculator").animate({bottom:'-50px'},2000);
		  $(".calculator").animate({bottom:'-170px'}, 1000);
		});   
		
		$(".calculator").hover(function(){
		  $(".calculator").animate({bottom:'-50px'});},
		  function () {$(".calculator").animate({bottom:'-170px'});
		});
	</script>  
  </body>
</html>
       