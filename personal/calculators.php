    <?php include 'header.php' ?>
    
    <div id="title-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active doublepadv">
            <div class="gradientTop">
           </div>
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 alignCenter">
                    	<img src="../img/icon-calculator.png" />Calculators
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
        
    <div id="calculators" class="container">
        <div class="intro row">
        	<div class="col-md-1">
            </div>
            <div class="col-md-10 dottedBorder doublepadv alignCenter">
                Try our loan calculator to see how much a mortgage might cost, or check to see how much you can afford to borrow for a new car, in four easy steps. 
           </div>
           <div class="col-md-1">
           </div>
        </div>
    	<div class="row doublepadv">
        	<div class="col-md-6 calcWrap doublepad">
            	<div class="stdpadb dottedBorder">
                    <img class="pull-left stdmarr" src="../img/icon-loan.png" /><h1 class="pageTitle blue">Loan Calculator</h1>
              </div><br />
                <form role="form">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Total</label>
                    <input type="total" class="form-control" id="exampleInputEmail1" placeholder="0">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Monthly Cost</label>
                    <input type="monthly" class="form-control" id="exampleInputPassword1" placeholder="0">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Term</label>
                    <input type="term" class="form-control" id="exampleInputPassword1" placeholder="Number of years">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Interest</label>
                    <input type="term" class="form-control" id="exampleInputPassword1" placeholder="% (APR)">
                  </div>
                  <br />
                  <a href="#" class="button">Calculate Now</a>
                </form><br />
            <h4 class="blue"><strong>Loan Calculator Guidance</strong></h4>
             <div class="blueWrap">
              <div class="stdpad">
                    <ul class="numberCircles">
                    		<li class="one">Put the amount you want to borrow in the total box,
                            or the amount you can afford to pay each month in the
                            monthly cost box.</li>
                         <li class="two">Put the term of the loan in the term box.</li>
                         <li class="three">Put the interest rate (APR) in the interest box.</li>
                         <li class="four">Press Calculate Now.</li>
                    </ul>
              </div>
           </div>
           </div>
           <div class="col-md-6 calcWrap doublepad">
            	<div class="stdpadb dottedBorder">
                    <img class="pull-left stdmarr" src="../img/icon-savings.png" /><h1 class="pageTitle blue">Savings Calculator</h1>
              </div><br />                <form role="form">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Total</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="0">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Monthly Contribution</label>
                    <input type="monthly" class="form-control" id="exampleInputPassword1" placeholder="0">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Term</label>
                    <input type="term" class="form-control" id="exampleInputPassword1" placeholder="Number of years">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Interest</label>
                    <input type="term" class="form-control" id="exampleInputPassword1" placeholder="% (Gross)">
                  </div>
                  <br />
                  <a href="#" class="button">Calculate Now</a>
                </form><br />
            <h4 class="blue"><strong>Savings Calculator Guidance</strong></h4>
             <div class="blueWrap">
              <div class="stdpad">
                    <ul class="numberCircles">
                    		<li class="one">Put the amount you want to save for in the total box, 
                            or the amount you can afford to save each month in the monthly 
                            contribution box. </li>
                         <li class="two">Put the term of the savings plan in the term box. </li>
                         <li class="three">Put the gross interest rate in the interest box. </li>
                         <li class="four">Press Calculate Now. </li>
                    </ul>
              </div>
           </div>
      		 </div>
    </div>
 </div>
    
    <?php include 'footer.php' ?>
   