    <?php include 'header.php' ?>
    
    <div id="services-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active quadpadb">
            <div class="gradientTop">
           </div>
        	<div class="container">
            	  <div class="row">
                  <div class="col-md-12 alignCenter">
                  	<h1>Services</h1>
                    <p>Click on one of the icons below to see our range of services</p><br />
                  </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                    	<ul class="servicesBanner clearfix">
                        	<li><a data-toggle="tooltip" title="Consultancy" class="servicesLink" 
                            href="#consultancy">
                            <img src="../img/icon-consultancy.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Equity Release" class="servicesLink" 
                            href="#equity">
                            <img src="../img/icon-equity.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Inheritance" class="servicesLink" 
                            href="#inheritance">
                            <img src="../img/icon-inheritance.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Long Term Care" class="servicesLink" 
                            href="#longTerm">
                            <img src="../img/icon-longterm.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Mortgages" class="servicesLink" 
                            href="#mortgages">
                            <img src="../img/icon-home.png" /></a></li>
                        	<li class="startSecondRow"><a data-toggle="tooltip" title="Pensions" 
                            class="servicesLink" href="#pensions">
                         <img src="../img/icon-pensions.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Private Investments" class="servicesLink" 
                            href="#investments">
                            <img src="../img/icon-investments.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Protection" class="servicesLink" 
                            href="#protection">
                            <img src="../img/icon-protection.png" /></a></li>
                        	<li><a data-toggle="tooltip" title="Retirement" class="servicesLink" 
                            href="#retirement">
                            <img src="../img/icon-retirement.png" /></a></li>
                     </ul>
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
    
    <div id="featurePods" class="container">
        <div class="row doublepadv">
            <div id="demoTab" class="resp-vtabs">          
                <ul class="resp-tabs-list">
                    <li id="consultancy-tab">Consultancy</li>
                    <li id="equity-tab">Equity Release</li>
                    <li id="inheritance-tab">Inheritance</li>
                    <li id="longTerm-tab">Long Term Care</li>
                    <li id="mortgages-tab">Mortgages</li>
                    <li id="pensions-tab">Pensions</li>
                    <li id="investments-tab">Private Investments</li>
                    <li id="protection-tab">Protection</li>
                    <li id="retirement-tab">Retirement</li>
                </ul> 
        
                <div class="resp-tabs-container">                                                        
                    <div id="consultancy-content">
                        <h1 class="blue">Consultancy</h1>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lorem leo, 
                        malesuada quis facilisis nec, blandit eget nisi. Sed facilisis rhoncus elit et 
                        rhoncus. Maecenas ac arcu tincidunt, sodales arcu vel, tempus nisi. Maecenas 
                        egestas quam quis nulla tempor, eget luctus dui placerat. Mauris gravida 
                        feugiat massa vitae molestie. Mauris non sapien leo. Etiam dui urna, malesuada 
                        pharetra molestie id, gravida id magna.</p>

							<p>Etiam suscipit est sit amet laoreet lacinia. Suspendisse sit amet vulputate 
                            turpis. Maecenas eros turpis, tristique ac tempus et, commodo eget ligula. 
                            Quisque id nisl nec nibh posuere elementum non vel nunc. Sed mollis risus 
                            ut odio porta vulputate. Pellentesque convallis et libero nec malesuada. 
                            Nullam vitae urna bibendum, pharetra mi ut, euismod tortor. In hac 
                            habitasse platea dictumst. Maecenas sodales aliquam pulvinar.</p>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="equity-content">
                        <h1 class="blue">Equity Release</h1>
                        <p>A complex range of products, available to homeowners aged 55 and 
                        above. There are many different options in the market place and our 
                        highly experienced, fully independent advisers will work with you, 
                        and your family if required, to provide you with the best solution 
                        to suit your personal circumstances, as follows:</p><br />
                        <h5>Lifetime Mortgage</h5>
                        <p>A loan secured on the borrower’s home for his/her lifetime. 
                        No monthly payments are made, the interest is added to the debt 
                        throughout the term of the loan, which is then repaid by selling 
                        the property when the borrower dies or moves into long term care. 
                        The borrower remains the owner, and retains the responsibilities 
                        and costs of home ownership.</p>
						  <h5>Interest Only</h5>
                        <p>A loan secured on the borrower’s home. Interest payments are made 
                        each month to the lender until the borrower leaves the property. 
                        The debt does not normally increase during the term.</p>
						  <h5>Home reversion</h5>
                        <p>The borrower sells all or part of their home to a third party, 
                        normally a reversion company. This means all or part of their 
                        home belongs to someone else. In return, the borrowers receive a 
                        regular income or cash lump sum, or both. The borrower continues 
                        to live in the property for as long as they wish.</p>
						  <h5>Shared Appreciation Mortgage</h5>
                        <p>The lender loans the borrower a capital lump sum in return 
                        for a share of the future increase in property value. The borrower 
                        retains the right to live in the property  until death.</p><br />
                        <p>It is very important that you take experienced, independent 
                        advice before making a permanent decision which will affect your 
                        finances for the rest of your life. A lady adviser is available, 
                        if you prefer.</p><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="inheritance-content">
                        <h1 class="blue">Inheritance</h1>
                        <p>We can give advice on ways of reducing a persons inheritance tax liability.  Advice will involve the subject of outright gifts using a persons annual exemptions and investments into Trust funds.  For couples we can also arrange joint life, second death insurance plans to provid the funds to meet the tax on their estate.</p>

<p><i>The Financial Services Authority does not regulate taxation and trust advice.</i></p><br />
						<a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="longTerm-content">
                        <h1 class="blue">Long Term Care</h1>
                        <p>For a number of years we have been providing advice on long term care planning. We listen to our clients’ concerns and fully explain the options available.  We work with clients, and individuals who hold Powers of Attorney, to assist in arranging fees for care and nursing which can be very high, and can offer a guaranteed care fees plan.  This, in essence, provides a tax free payment direct to the care home for the remainder of the resident’s life.  In addition the payment can be index-linked to allow for any increase in care home fees.</p>
                       <p>Martin Cornell is our qualified adviser and is an accredited member of the Society of Later Life Advisers.</p>
                       <a href="http://societyoflaterlifeadvisers.co.uk/">Visit SOLLA Website</a>
                       <br /><br />
                       <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="mortgages-content">
                        <h1 class="blue">Mortgages</h1>
                        <p>Our highly experienced, independent advisers can guide you in all areas
                        of property funding - residential mortgages, re-mortgages, or buy-to-let
                        mortgages</p>
                        <p>Here's what you can expect from our mortgage service:</p>
                        <ul>
                        		<li>Whole of Market research</li>
                            <li>Exclusive products</li>
                            <li>Bespoke total cost analysis (we don't just look at the headline
                            mortgage rate).</li>
                            <li>A dedicated fully qualified adviser, from start to finish</li>
                            <li>Specialist buy to let mortgage advice, to help you get a better
                            return</li>
                        </ul>
                        <p>We can help you find the mortgage deal that suits you best, after
                        taking account of your individual needs.</p><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="pensions-content">
                        <h1 class="blue">Pensions</h1>
                        <p>After the family home, a person's pension fund is often his or her
                        second most significant asset.</p>
                        <p>Are you certain that your pension fund will provide you with the
                        retirement you wish for?</p>
                        <p>Changes to pension legislation have made planning for retirement
                        very complicated and we can assist by:</p>
                        <ul>
                        		<li>Explaning how any existing plans that you have work.</li>
                             <li>Reviewing your pension plans in relation to your retirement objectives.</li>
                             <li>Managing these plans going forward and ensuring that you maximise your
                             retirement income when you retire.</li>
                        </ul><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="investments-content">
                        <h1 class="blue">Private Investments</h1>
                        <p>Clayden Financial utilise sophisticated systems, which enable us to provide a portfolio of investments that aim to complement each person’s attitude to investment risk.  As a result we will recommend to each individual customer a portfolio tailor-made to suit their own specific requirements, and attitude to investment risk.</p>
                        
                        <p>Common investments include:</p>

						  <ul>
                          	<li>ISA's</li>
                            <li>Unit trusts/Open ended investment companies</li>
                            <li>Investment bonds</li>
                            <li>Investment trusts</li>
                       </ul>
                       
                       <p>Our aim is to help you achieve your goals in a world of investment choice.
                       You may require advice for growth or income, or a combination of both.</p>
                       
                       <h5>Are you investing for a specific purpose?</h5>
                       
                       <ul>
                       	<li>A better potential return?</li>
                        	<li>Wedding, holiday, children's savings/schooling?</li>
                         <li>Greater cash and income in retirement?</li>
                       </ul>
                       
                       <h5>Whatever your investment needs we aim to assist you.</h5>
                       
                       <p>It should be remembered that past performance is no guarantee of future
                       returns and the price of units and the income from them can fall as well as
                       rise. The value of unit-linked investment is not guaranteed and you may not
                       get back the full amount investment.</p><br />
                       
                       <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                       
                        
                    </div>
                    <div id="protection-content">
                        <h1 class="blue">Protection</h1>
                        <h5>Protecting your family</h5>
                        <p>We offer a free initial consultation, where we will gain a full 
                        appreciation of your particular situation. If you wish, we can then 
                        research the market for competitively priced products and provide our 
                        recommendations to you.  We can arrange for the following products:-</p>
                        <h5>Mortgage Protection</h5>
                        <p>The mortgage is often a family’s largest commitment and single or 
                        joint policies can cover the amount of the outstanding mortgage in 
                        the event of an unforeseen event.</p>
                        <h5>Team Assurances/Whole of Life</h5>
                        <p>To cover a short-term loan, or a policy to cover an individual’s 
                        life, “whole of life” is often used in planning for inheritance 
                        tax – including the use of Trusts.</p>
                        <h5>Family Income Benefits</h5>
                        <p>If you were to die or become seriously ill, your family’s financial 
                        situation could worsen significantly. Many people find that their 
                        mortgage is protected but income for household expenses is not. 
                        For the price of a daily cup of coffee, we could provide a high level 
                        of income assurance for your family. Family income assurance can give 
                        you financial security by providing a guaranteed monthly income – 
                        free of tax – should you die, or become critically ill.</p>
                        <h5>Protecting Your Income</h5>
                        <p>We can arrange a policy that is designed to provide an income 
                        for the rest of your working life. The policy will pay you an income 
                        if you are unable to continue working.</p>
                        <h5>Private Medical Insurance</h5>
                        <p>A policy specially designed to cover all of your family members 
                        for private medical insurance. This is an alternative to using the NHS.</p><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="retirement-content">
                        <h1 class="blue">Retirement</h1>
                        <p>Most people retire on less cash and income than they would like, whether you are employed, self-employed or a company director.  Therefore it is vitally important to obtain the right retirement planning advice as early as possible.  We can advise you on suitable plans depending on your current needs and circumstances. We can also review any existing pension plans you may have.</p><br />
                        
                        <h5>Personal/Stakeholder Pension</h5>
                        <ul>
                        		<li>Self invested personal pensions</li>
                             <li>Pension transfers</li>
                             <li>Group pension schemes</li>
                        </ul>
                        
                        <h5>At retirement age we can advise on:</h5>
                        <ul>
                        		<li>Annuities</li>
                             <li>Income drawdown and other options</li>
                             <li>Accessing tax-free cash</li>
                        </ul>
                        
                        <h5>Key facts to consider at this time are:</h5>
                        <ul>
                        		<li>How much income you require as your target</li>
                             <li>What effect inflation may have</li>
                             <li>Do you need flexibility</li>
                             <li>Access to cash without drawing your income</li>
                             <li>A guaranteed/variable/fixed/index linked income</li>
                        </ul>
							<br />
							<p>We can provide comprehensive pensions advice on a personal or business basis.</p>
                         <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                </div>
            </div>    	 
    	 </div>
    </div>
    
    
    
    <?php include 'footer.php' ?>
   