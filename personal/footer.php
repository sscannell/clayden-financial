    <div id="clients">
    	<div class="container stdpadv alignCenter">
            <div class="row">
                <div class="col-md-12">
                    <img src="../img/clients.jpg" />
               </div>
           </div>
       </div>
    </div>
    
    <footer>
    	<div class="container doublepadv">
        	<div class="row">
            	<div class="col-md-3 col-sm-3 col-xs-6">	
               	<h6>Our Services</h6>
                	<ul>
                    <li><a href="#">Consultancy</a></li>
                    <li><a href="#">Equity Release</a></li>
                    <li><a href="#">Inheritance</a></li>
                    <li><a href="#">Long Term Care</a></li>
                    <li><a href="#">Mortgages</a></li>
                  </ul>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
              		<h6>&nbsp;</h6>
                	<ul>
                    <li><a href="#">Pensions</a></li>
                    <li><a href="#">Private Investments</a></li>
                    <li><a href="#">Protection</a></li>
                    <li><a href="#">Retirement</a></li>
                  </ul>
              </div>
              <div class="clearBoth"></div>
              <div class="col-md-3 col-sm-3 col-xs-6">
              		<h6>Contact Us</h6>
                	<ul>
                    <li>Clayden Financial</li>
                    <li>1 Constable Court</li>
                    <li>The Street, Belstead</li>
                    <li>Ipswich, IP8 3LY</li>
                    <li class="orange">(01473) 730090</li>
                  </ul>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
              		<h6>Keep in Touch</h6><br />
                    <form role="form">
                      <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" 
                        placeholder="Enter email address">
                      </div>
                        <a href="#"><img src="../img/footer-twitter.png" /></a>
                        <a href="#"><img src="../img/footer-facebook.png" /></a>
                      <a href="#" class="button pull-right">Subscribe</a>
                    </form>
              </div>
           </div>
       </div>
       <div class="copyright">
       		<div class="container">
            	<div class="row">
                	<div class="col-md-6">
                    Copyright © 2013 Clayden Financial. All rights reserved.
                  </div>
                  <div class="col-md-6">
                  </div>
              </div>
           </div>
       </div>
    </footer>
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.js"></script>
    
    <script src="../js/easyResponsiveTabs.js"></script>
    
    <script src="../js/services.js"></script>
    
    <script>
		$(document).ready(function(){
		  $(".calculator").animate({bottom:'-50px'},2000);
		  $(".calculator").animate({bottom:'-170px'}, 1000);
		});   
		
		$(".calculator").hover(function(){
		  $(".calculator").animate({bottom:'-50px'});},
		  function () {$(".calculator").animate({bottom:'-170px'});
		});
		
		$(".dropdown").hover(function(){
			$(".dropdown").removeClass("open");
			$(this).addClass("open");
			},function () {
		    $(this).removeClass("open");
		});
		
		$(".calcWrap").hover(function(){
			$(this).addClass("blueBack");
			},function(){
			$(this).removeClass("blueBack");
		});
								
    </script> 
    
    <script>
		$('#demoTab').easyResponsiveTabs();    
    </script>
        
  </body>
</html>
