    <?php include 'header.php' ?>

    <div id="title-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active doublepadv">
            <div class="gradientTop">
           </div>
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 alignCenter">
                    	<img src="../img/icon-news.png" />News
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
    
    <div id="news" class="container">
        <div class="row doublepadv">
            <div id="demoTab" class="resp-vtabs">  
                <ul class="resp-tabs-list">
                    <li>June 2014<span class="numberCount">04</span></li>
                    <li>May 2014<span class="numberCount">12</span></li>
                    <li>April 2014<span class="numberCount">18</span></li>
                    <li>March 2014<span class="numberCount">09</span></li>
                    <li>February 2014<span class="numberCount">13</span></li>
                    <li>January 2014<span class="numberCount">21</span></li>
                </ul> 
        
                <div class="resp-tabs-container">                                                        
                    <div>
                        <div class="media stdpadv">
                          <div class="circleImg pull-left">
                            <img class="media-object" src="../img/case1.jpg" />
                          </div>
                          <div class="media-body">
                              <span class="numberCount">26th Jun</span>
                            <h2 class="media-heading blue">Blog Post Title One</h2>                  
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin 
                            dignissim diam non velit vehicula, sit amet condimentum risus consectetur.</p>
                            <a href="#" class="pull-right button">Read More</a>
                          </div>
                        </div>
                        <div class="media stdpadv">
                          <div class="circleImg pull-left">
                            <img class="media-object" src="../img/case1.jpg" />
                          </div>
                          <div class="media-body">
                              <span class="numberCount">21st Jun</span>
                            <h2 class="media-heading blue">Blog Post Title Two</h2>                  
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin 
                            dignissim diam non velit vehicula, sit amet condimentum risus consectetur.</p>
                            <a href="#" class="pull-right button">Read More</a>
                          </div>
                        </div>
                        <div class="media stdpadv">
                          <div class="circleImg pull-left">
                            <img class="media-object" src="../img/case1.jpg" />
                          </div>
                          <div class="media-body">
                              <span class="numberCount">19th Jun</span>
                            <h2 class="media-heading blue">Blog Post Title Three</h2>                         
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin 
                            dignissim diam non velit vehicula, sit amet condimentum risus consectetur.</p>
                            <a href="#" class="pull-right button">Read More</a>
                          </div>
                        </div>
                        <div class="media stdpadv">
                          <div class="circleImg pull-left">
                            <img class="media-object" src="../img/case1.jpg" />
                          </div>
                          <div class="media-body">
                              <span class="numberCount">12th Jun</span>
                            <h2 class="media-heading blue">Blog Post Title Four</h2>                  
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin 
                            dignissim diam non velit vehicula, sit amet condimentum risus consectetur.</p>
                            <a href="#" class="pull-right button">Read More</a>
                          </div>
                        </div>
                    </div>
                </div>
            </div>    	 
    	 </div>
    </div>
    
    
    
    <?php include 'footer.php' ?>
   