    <?php include '../business/header.php' ?>
    
    <div id="title-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active doublepadv">
            <div class="gradientTop">
           </div>
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 alignCenter">
                    	<img src="../img/icon-contact.png" />Contact
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
    
<iframe id="googleMap" width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?source=embed&amp;f=q&amp;hl=en&amp;geocode=&amp;q=Clayden+Financial,+1+Constable+Court,+The+Street,+Belstead,+Ipswich+IP8+3LY&amp;ie=UTF8&amp;t=m&amp;ll=52.029683,1.108932&amp;spn=0.042246,0.016994&amp;z=13&amp;output=embed"></iframe><br /><small></small>               
    
    <div id="contact" class="container">
    	<div class="row doublepadv">
        	<div class="col-md-6">
            	<h4 class="blue"><strong>Enquiry Form</strong></h4><br />
                <form role="form">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Surname</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Subject</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Message</label>
                    <textarea class="form-control" rows="3"></textarea>
                  </div>
                  <br />
                  <a href="#" class="button">Submit Enquiry</a>
                </form>
           </div>
           <div class="col-md-6">
            <h4 class="blue"><strong>Contact Details</strong></h4>
             <div class="blueWrap">
              <div class="stdpad">
              		<div class="row">
                    	<div class="col-md-6">
                          <p><strong>Address</strong><br />
                          Clayden Financial<br />1 Constable Court<br />
                          The Street, Belstead<br />Ipswich<br />IP8 3LY</p>
                     </div>
                    	<div class="col-md-6">
                          <p><strong>Telephone</strong><br />
                          (01473) 730090</p>
                          <p><strong>Fax</strong><br />
                          (01473) 730092</p>
                          <p><strong>Email</strong><br />
                          <a href="mailto:advice@claydens.com">advice@claydens.com
                          </a></p>
                     </div>
                  </div>
              </div>
           </div>
           <br />
            <h4 class="blue"><strong>Further Directions</strong></h4>
             <div class="blueWrap">
              <div class="stdpad">
                    <p>If travelling via A137, once in Belstead village, 
                    approx. 100yds past Street Farm (on right), there is 
                    a sharp right hand bend. At this bend turn left on 
                    to a track.</p>

					<p>If travelling from other directions, approx. 400yds past 
                    the Village Hall (on the left), there is a sharp left 
                    hand bend. Just before this bend turn right on to 
                    a track.</p>

					<p>Constable Court is the converted farm building on the 
                    left past Blacksmith's Cottage.</p>
              </div>
           </div>
       </div>
    </div>
 </div>
    
    <?php include '../business/footer.php' ?>
   