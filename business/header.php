<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clayden Financial</title>

    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/easy-responsive-tabs.css" rel="stylesheet"> 
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default" role="navigation">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="../business/index.php"><img src="../img/logo.jpg" /></a>
                    </div>
                
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>Business</strong><b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="../personal/index.php">Switch to Personal Website</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="../business/team.php" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="../business/team.php">The Team</a></li>
                            <li><a href="../business/testimonials.php">Testimonials</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="../business/services.php" class="dropdown-toggle">Services <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="../business/services.php">Pensions</a></li>
                            <li><a href="../business/services.php">Insurance</a></li>
                            <li><a href="../business/services.php">Investments</a></li>
                            <li><a href="../business/services.php">Consultancy</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="../business/news.php" class="dropdown-toggle">News & Events <b class="caret">
                          </b></a>
                          <ul class="dropdown-menu">
                            <li><a href="../business/news.php">News</a></li>
                            <li><a href="../business/events.php">Events</a></li>
                          </ul>
                        </li>
                        <li><a href="../business/connections.php">Professional Connections</a></li>
                        <li><a href="../business/info.php">Helfpul Links</a></li>
                        <li><a href="../business/contact.php">Contact</a></li>
                        <li class="dropdown searchDrop">
                            <a href="#" class="dropdown-toggle">
                            <span class="iconSearch"></span></a>
                              <ul class="dropdown-menu">
                                    <form class="navbar-form navbar-left" role="search">
                                      <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search Clayden Financial">
                                      </div>
                                    </form>
      							</ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
