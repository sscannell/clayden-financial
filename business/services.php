    <?php include '../business/header.php' ?>
    
    <div id="businessServicesBanner">
        <div id="services-carousel" class="carousel slide" data-ride="carousel">
        
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active quadpadb">
                <div class="gradientTop">
               </div>
                <div id="businessServices" class="container">
                      <div class="row">
                      <div class="col-md-12 alignCenter">
                        <h1>Services</h1>
                        <p>Click on one of the icons below to see our range of services</p><br />
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="servicesBanner clearfix">
                                <li><a data-toggle="tooltip" title="Pensions" 
                                class="servicesLink" href="#pensions">
                             <img src="../img/icon-pensions.png" /></a></li>
                                <li><a data-toggle="tooltip" title="Insurance" class="servicesLink" 
                                href="#insurance">
                                <img src="../img/icon-umbrella.png" /></a></li>
                                <li><a data-toggle="tooltip" title="Investments" class="servicesLink" 
                                href="#investments">
                                <img src="../img/icon-investments.png" /></a></li>
                                <li><a data-toggle="tooltip" title="Consultancy" class="servicesLink" 
                                href="#consultancy">
                                <img src="../img/icon-consultancy.png" /></a></li>
                         </ul>
                      </div>
                   </div>
               </div>
            </div>
          </div>
        
        </div>
    </div>
    
    <div id="featurePods" class="container">
        <div class="row doublepadv">
            <div id="demoTab" class="resp-vtabs">          
                <ul class="resp-tabs-list">
                    <li id="pensions-tab">Pensions</li>
                    <li id="insurance-tab">Insurance</li>
                    <li id="investments-tab">Investments</li>
                    <li id="consultancy-tab">Consultancy</li>
                </ul> 
        
                <div class="resp-tabs-container">             
                    <div id="pensions-content">
                        <h1 class="blue">Pensions</h1>
                        <p>After the family home, a person's pension fund is often his or her
                        second most significant asset.</p>
                        <p>Are you certain that your pension fund will provide you with the
                        retirement you wish for?</p>
                        <p>Changes to pension legislation have made planning for retirement
                        very complicated and we can assist by:</p>
                        <ul>
                        		<li>Explaning how any existing plans that you have work.</li>
                             <li>Reviewing your pension plans in relation to your retirement objectives.</li>
                             <li>Managing these plans going forward and ensuring that you maximise your
                             retirement income when you retire.</li>
                        </ul><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="insurance-content">
                        <h1 class="blue">Insurance</h1>
                        <p>After the family home, a person's pension fund is often his or her
                        second most significant asset.</p>
                        <p>Are you certain that your pension fund will provide you with the
                        retirement you wish for?</p>
                        <p>Changes to pension legislation have made planning for retirement
                        very complicated and we can assist by:</p>
                        <ul>
                        		<li>Explaning how any existing plans that you have work.</li>
                             <li>Reviewing your pension plans in relation to your retirement objectives.</li>
                             <li>Managing these plans going forward and ensuring that you maximise your
                             retirement income when you retire.</li>
                        </ul><br />
                        <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                    <div id="investments-content">
                        <h1 class="blue">Investments</h1>
                        <p>Clayden Financial utilise sophisticated systems, which enable us to provide a portfolio of investments that aim to complement each person’s attitude to investment risk.  As a result we will recommend to each individual customer a portfolio tailor-made to suit their own specific requirements, and attitude to investment risk.</p>
                        
                        <p>Common investments include:</p>

						  <ul>
                          	<li>ISA's</li>
                            <li>Unit trusts/Open ended investment companies</li>
                            <li>Investment bonds</li>
                            <li>Investment trusts</li>
                       </ul>
                       
                       <p>Our aim is to help you achieve your goals in a world of investment choice.
                       You may require advice for growth or income, or a combination of both.</p>
                       
                       <h5>Are you investing for a specific purpose?</h5>
                       
                       <ul>
                       	<li>A better potential return?</li>
                        	<li>Wedding, holiday, children's savings/schooling?</li>
                         <li>Greater cash and income in retirement?</li>
                       </ul>
                       
                       <h5>Whatever your investment needs we aim to assist you.</h5>
                       
                       <p>It should be remembered that past performance is no guarantee of future
                       returns and the price of units and the income from them can fall as well as
                       rise. The value of unit-linked investment is not guaranteed and you may not
                       get back the full amount investment.</p><br />
                       
                       <a class="button">Enquire about Service</a>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                       
                        
                    </div>
                    <div id="consultancy-content">
                        <h1 class="blue">Consultancy</h1>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lorem leo, 
                        malesuada quis facilisis nec, blandit eget nisi. Sed facilisis rhoncus elit et 
                        rhoncus. Maecenas ac arcu tincidunt, sodales arcu vel, tempus nisi. Maecenas 
                        egestas quam quis nulla tempor, eget luctus dui placerat. Mauris gravida 
                        feugiat massa vitae molestie. Mauris non sapien leo. Etiam dui urna, malesuada 
                        pharetra molestie id, gravida id magna.</p>

							<p>Etiam suscipit est sit amet laoreet lacinia. Suspendisse sit amet vulputate 
                            turpis. Maecenas eros turpis, tristique ac tempus et, commodo eget ligula. 
                            Quisque id nisl nec nibh posuere elementum non vel nunc. Sed mollis risus 
                            ut odio porta vulputate. Pellentesque convallis et libero nec malesuada. 
                            Nullam vitae urna bibendum, pharetra mi ut, euismod tortor. In hac 
                            habitasse platea dictumst. Maecenas sodales aliquam pulvinar.</p>
                        <a class="backTop" href="#backtotop">Back to Top</a>
                    </div>
                </div>
            </div>    	 
    	 </div>
    </div>
    
    
    
    <?php include '../business/footer.php' ?>
   