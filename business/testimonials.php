    <?php include '../business/header.php' ?>
    
    <div id="title-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active doublepadv">
            <div class="gradientTop">
           </div>
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 alignCenter">
                    	<img src="../img/icon-testimonials.png" />Testimonials
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
    
    
    <!--	<div class="blueWrap">
        	<div class="container">
                <div class="row doublepadv">
                    <div class="col-md-12">
                        <p>Our five advisers at Clayden Financial collectively have many years of experience in Financial Services, and some of our advisers hold Advanced Financial Planning Certificates, Chartered Insurance Institute membership and Long Term Care, Equity Release and full mortgage qualifications.  We like to build and maintain good customer relationships with our clients and offer a flexibility to work for you in the way you prefer.</p>
                        <p>We have four experienced office administrators providing support to the advisers on a full time basis.</p>
                       <p>Our offices, with car parking facilities, are located in Belstead village, in the beautiful Suffolk countryside on the outskirts of Ipswich, with easy access to the A14 and A12 trunk routes.</p>
                   </div>
               </div>
            </div>
       </div>-->
       
       <div class="container">
            <div class="row doublepadv">
            	<div class="col-md-1">
               </div>
                <div class="col-md-10">
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Ruth McAdams</span>
                      </div>
                      <div class="media-body">
                        <p>Thank you so much for meeting me on Monday.  It was kind of you to spare so much time.  Our discussions were very helpful and I came away feeling ten foot tall!  This year has been a watershed for me, I have gained confidence and you have been part of that.  I shall be eternally grateful for all your help to Mum (re Dad) and on-going.  I hope you have a relaxing break and all you wish for in 2014.  </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">George & Steph Yuill</span>
                      </div>
                      <div class="media-body">
                        <p>I just wanted to send you a note to thank you for your guidance, which has provided us with just what we wanted. Our funds arrived on Monday, we changed our car yesterday, and are off in the new one on holiday tomorrow!  Again, thanks G and S</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">John Knox</span>
                      </div>
                      <div class="media-body">
                        <p>Some time ago, an old friend and ex-colleague of mine recommended your company to me when I was talking to him about retiring. ….  He said you saved him a lot of money and that I would be wise to take your advice.  How right he was.  I have been dealing with Karen Last and occasionally with Chris McClone.  I found Karen to be a very friendly, understanding and thoroughly professional person.  I had some ideas about one of my pensions, but Karen explained that some of my ideas would cost me thousands.   I quickly took her advice.  Chris McClone kept me updated when Karen wasn’t available.  They are both a credit to your company.  I have already recommended you to other people who needed advice.  I thank you all for arranging my financial matters.  All monies arrived into my bank account as predicted.  Thanks to Karen I think I will have a happy retirement. JHK </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Stuart & Issy Cracknell</span>
                      </div>
                      <div class="media-body">
                        <p>Firstly, we just want to say THANK YOU for looking into our pensions and insurances.  We have never had anyone explain them to us like you did, all they want to do is sell us one of their products.  We both thought how brilliant you were in finding out the information and talking to us in a very straightforward way.  It certainly has put our minds at rest with our finances.  More cake?  We are happy to pay for your financial advice as we know we’ll get honest and down to earth advice. S and I </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Lorraine Muldoon</span>
                      </div>
                      <div class="media-body">
                        <p>Your attention to detail is outstanding …….  so glad that you are looking after my finances.  I feel that I couldn’t be in safer hands. L </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Andra Tongue</span>
                      </div>
                      <div class="media-body">
                        <p>I am extremely grateful for all your help , advice and “metaphoric hand-holding” and not to forget Christine’ valuable input.  At present  a weight has been lifted and I can finish the work on the house.  Many many thanks – much appreciated. A </p>
                      </div>
                    </div>
               </div>
               <div class="col-md-1">
               </div>
           </div>
       </div>
  
    
    
    <?php include '../business/footer.php' ?>
   