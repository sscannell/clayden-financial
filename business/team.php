    <?php include '../business/header.php' ?>
    
    <div id="title-carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active doublepadv">
            <div class="gradientTop">
           </div>
        	<div class="container">
                <div class="row">
                	<div class="col-md-12 alignCenter">
                    	<img src="../img/icon-team.png" />The Team
                  </div>
               </div>
           </div>
        </div>
      </div>
    
    </div>
           
       <div class="container">
        <div class="intro row">
        	<div class="col-md-1">
            </div>
            <div class="col-md-10 dottedBorder doublepadv alignCenter">
               <p>Our five advisers at Clayden Financial collectively have many years of experience in Financial Services, and some of our advisers hold Advanced Financial Planning Certificates, Chartered Insurance Institute membership and Long Term Care, Equity Release and full mortgage qualifications.  We like to build and maintain good customer relationships with our clients and offer a flexibility to work for you in the way you prefer.</p>
               <p>We have four experienced office administrators providing support to the advisers on a full time basis. </p> 
           </div>
           <div class="col-md-1">
           </div>
        </div>
            <div class="row doublepadv">
            	<div class="col-md-1">
               </div>
                <div class="col-md-10">
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case1.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">Leigh Clayden</h3>
                        <h5 class="blue"><strong>CeMAP, Dip PFS</strong></h5>
                        <p>Leigh started the company in 1994, having spent 18 years with a major UK insurance company as a senior sales consultant.  He holds additional specialist qualifications set by the Chartered Insurance Institute, which enable him to offer the highest level of advice on pension transfers, pension fund withdrawal and all areas of retirement planning, including Self Invested Personal Pensions (SIPP).  This enables the firm to offer advice in all areas of SIPP planning including commercial planning.  Leigh also specialises in investment and inheritance tax planning advice.  Leigh’s daughter Gemma works as an Administrator and wife Jane is the firm’s Company Secretary.</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case2.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">Martin Cornell</h3>
                        <h5 class="blue"><strong>APFS</strong></h5>
                        <p>Martin joined the firm in 2008, having become an Independent Financial Adviser in 2005.  Prior to this he had spent 21 years in financial services working for well-known UK companies such as Standard Life, Aviva and Barclays.  He is very highly skilled, holding specialist qualifications in pensions, investments, taxation, trusts, long term care and equity release, enabling him to give full and in-depth advice.  He has SOLLA accreditation (Society of Later Life Advisers).  Those advisers who have taken the further steps necessary to become independently accredited by SOLLA can offer the added reassurance that they offer the practical help and guidance needed to help clients make the right decisions at the right time.    </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case3.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">John Brown</h3>
                        <h5 class="blue"><strong>ACII, Cert PFS</strong></h5>
                        <p>John joined Clayden Financial in 1995 as a senior consultant and has now spent over forty years in the insurance and financial services industry.  He specialises in providing advice to individuals on life assurance, retirement planning, investments, Lifetime Mortgages, tax planning and health insurance.  He is an Associate of the Chartered Insurance Institute as well as holding the Financial Planning Certificate of the Chartered Insurance Institute. </p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case4.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">Adrian Firth</h3>
                        <h5 class="blue"><strong>Dip PFS, CII (MP & ER), CeMAP</strong></h5>
                        <p>Adrian joined Clayden Financial in September 2013 after leaving a well-respected local accountancy practice where he was the Head of Financial Planning.</p>
                        <p>With over 20 years’ experience, Adrian has built a reputation for providing easy to understand, impartial advice on all aspects of retirement planning, investments and mortgages (both residential and Buy to Let).  He holds specialist qualifications in Pensions, Investments, Taxation, Mortgage Advice, Long Term Care, Equity Release and Lifetime Mortgages.  Adrian also holds a Diploma in Financial Planning and a Statement of Professional Standing from the Chartered Insurance Institute. He has written articles for the finance section of local publications and has a particular interest in ensuring his clients receive a high standard of personal service.   In his spare time Adrian enjoys travel and food (not necessarily in that order) and spending time with his wife Karen, family and friends.</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case2.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">Karen Last</h3>
                        <h5 class="blue"><strong>Adv CeMap, Dip FA</strong></h5>
                        <p>Karen has more than 30 years’ experience in the financial services industry.  She is passionate about building long-term relationships with clients by providing sound, practical advice which is not influenced by income or quick, fragile solutions.  She provides a personal and efficient service covering a wide range of advice.  Karen specialises in pre and post retirement planning for individuals, and offers advice to companies about their legal responsibilities in this area.  She can advise on how to make the most of your savings and also how to protect your family or business from the sad effects of death or serious illness. Karen is qualified to give advice on equity release mortgages which enable older clients to release funds from their property.</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="circleImg pull-left">
                        <img class="media-object" src="../img/case2.jpg" />
                      </div>
                      <div class="media-body">
                      <span class="numberCount">Contact by Email</span>
                        <h3 class="blue">Sean Parker</h3>
                        <h5 class="blue"><strong>Dip PFS IMC</strong></h5>
                        <p>Sean joined Clayden Financial in November 2012 as a paraplanner, a role he previously occupied at a large IFA in Norwich. He provides technical, research and report writing support to the team with respect to financial planning matters alongside his role as a point of contact for existing and new client enquiries in the absence of any member of the advice team. With specialist investment qualifications, and as part of the investment committee, he is also responsible for the ongoing monitoring and construction of the investment portfolios used by clients including the Clayden Financial Wealth Management proposition. </p>
                      </div>
                    </div>
               </div>
               <div class="col-md-1">
               </div>
           </div>
       </div>
  
    
    
    <?php include '../business/footer.php' ?>
   