    <?php include '../business/header.php' ?>
    
    <div id="main-carousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#main-carousel" data-slide-to="1"></li>
      </ol>
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item office01 active">
            <div class="gradientTopBlack">
           </div>
        	<div class="container">
                <div class="row">
                   <div class="col-md-6 col-sm-6">
                    <img class="bannerPerson" src="../img/business-person1.png" />
                   </div>
                    <div class="col-md-6 col-sm-6 quadpadv">
                        <div class="speechBubble">
                        	We've helped over<br /><em class="bold">100</em> local companies<br />
                            <div class="speechArrow">
                            </div>
                        </div>
                        <br /><br />
                        <span class="quoteName">Leigh Clayden<br /><strong>Director</strong></span>
                        <a href="#" class="pull-right bigButton">Browse our services</a>
                   </div>
               </div>
           </div>
           <div class="gradientBottomBlack">
           </div>
        </div>
        <div class="item office02">
            <div class="gradientTopBlack">
           </div>
        	<div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 quadpadv">
                        <div class="speechBubble">
                                <div style="margin-top:-30px;">
                             <span class="smallBannerText">Our team are here to</span><br />
                                provide personal<br />and friendly expertise<br />
                                <div class="speechArrowRight">
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <a href="#" class="bigButton">Browse our services</a>
                        <span class="quoteName pull-right">Karen Last<br /><strong>Job Title</strong></span>
                   </div>
                   <div class="col-md-6 col-sm-6">
                    <img class="bannerPerson" src="../img/business-person2.png" />
                   </div>
               </div>
           </div>
           <div class="gradientBottomBlack">
           </div>
        </div>
      </div>
    
      <!-- Controls -->
      <a class="left carousel-control" href="#main-carousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#main-carousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
    
    <div id="featurePods" class="container">
        <div class="row doublepadv">
            <div class="col-md-4">
            	  <div class="alignCenter">
                      <img src="../img/icon-trust.png" />
                 </div>
                <h2>Trust us, we're experts</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing 
               elementum turpis sit amet dictum. Maecenas euismod sit amet risus ut dictum. 
               Sed diam nisl, rhoncus eu arcu sit</p>
           </div>
           <div class="col-md-4">
            	  <div class="alignCenter">
                      <img src="../img/icon-stressfree.png" />
                 </div>
                <h2>Stress free advice</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing 
               elementum turpis sit amet dictum. Maecenas euismod sit amet risus ut dictum. 
               Sed diam nisl, rhoncus eu arcu sit</p>
           </div>
           <div class="col-md-4">
            	  <div class="alignCenter">
                      <img src="../img/icon-target.png" />
                 </div>
                <h2>We focus on helping you</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing 
               elementum turpis sit amet dictum. Maecenas euismod sit amet risus ut dictum. 
               Sed diam nisl, rhoncus eu arcu sit</p>
           </div>
        </div>
    </div>
    
    <div class="blueWrap backSpeech">
        <div id="message" class="container">
            <div class="row doublepadv">
                <div class="col-md-6">
                <h1>Personal and friendly expertise.</h1>
                <p>We provide a high quality service covering all forms of financial advice for private clients.  On joining our programme you can be confident that your financial plans are being reviewed regularly by a well-qualified and diligent financial adviser.  Most importantly, advice you are given will be completely impartial and without bias towards a particular product or supplier.  Our advisers and support staff are warm, friendly and very happy to answer questions.</p>
                <a class="button" href="#">Meet our experts</a>
               </div>
               <div class="col-md-6 alignCenter">
               	<img src="../img/staff-member.png" />
               </div>
            </div>
        </div>
    </div>
    
    <div id="caseStudies" class="container doublepadv">
    	<div class="row">
        	<div class="col-md-12">
                <h1>Don’t just take our word for it...</h1>
           </div>
        </div>
    	<div class="row doublemart">
        	<div class="col-md-6">
                <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Ruth McAdams</span>
                      </div>
                  <div class="media-body">
                    <p>“Thank you so much for meeting me on Monday. It was kind of you to spare so much time. Our discussions were very helpful and I came away feeling ten foot tall! This year has been a watershed for me, I have gained confidence and you have been part of that. I shall be eternally grateful for all your help to Mum (re Dad) and on-going. I hope you have a relaxing break and all you wish for in 2014. ” </p>
                  </div>
                </div>
                <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">George & Steph Yuill</span>
                      </div>
                  <div class="media-body">
                    <p>“I just wanted to send you a note to thank you for your guidance, which has provided us with just what we wanted. Our funds arrived on Monday, we changed our car yesterday, and are off in the new one on holiday tomorrow! Again, thanks G and S"</p>
                  </div>
                </div>
           </div>
           <div class="col-md-6">
                <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Stuart & Issy Cracknell</span>
                      </div>
                  <div class="media-body">
                    <p>“Firstly, we just want to say THANK YOU for looking into our pensions and insurances. We have never had anyone explain them to us like you did, all they want to do is sell us one of their products. We both thought how brilliant you were in finding out the information and talking to us in a very straightforward way. It certainly has put our minds at rest with our finances. More cake? We are happy to pay for your financial advice as we know we’ll get honest and down to earth advice. S and I"</p>
                  </div>
                </div>
                <div class="media">
                      <div class="circleText pull-left">
                        <span class="name">Lorraine Muldoon</span>
                      </div>
                  <div class="media-body">
                    <p>“Your attention to detail is outstanding ……. so glad that you are looking after my finances. I feel that I couldn’t be in safer hands. L"</p>
                  </div>
                </div>
           </div>
       </div>
    </div>
    
    <?php include '../business/footer.php' ?>
   