				
		$('.servicesBanner a').tooltip({trigger: 'manual'})
				
		$(".servicesLink").hover(function() {
			$(this).tooltip('show');
						
		},function() {
			$(this).tooltip('hide');

		});
		
						
		$(".servicesLink").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$(this).tooltip('toggle');
			$(this).addClass('active');
		});
		
		$("#consultancy-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#consultancy']").tooltip('toggle');
			$("a[href='#consultancy']").addClass('active');
		});
		
		$("#equity-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#equity']").tooltip('toggle');
			$("a[href='#equity']").addClass('active');
		});
		
		$("#inheritance-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#inheritance']").tooltip('toggle');
			$("a[href='#inheritance']").addClass('active');
		});
		
		$("#longTerm-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#longTerm']").tooltip('toggle');
			$("a[href='#longTerm']").addClass('active');
		});
		
		$("#mortgages-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#mortgages']").tooltip('toggle');
			$("a[href='#mortgages']").addClass('active');
		});
		
		$("#mortgages-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#mortgages']").tooltip('toggle');
			$("a[href='#mortgages']").addClass('active');
		});
		
		$("#pensions-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#pensions']").tooltip('toggle');
			$("a[href='#pensions']").addClass('active');
		});
		
		$("#investments-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#investments']").tooltip('toggle');
			$("a[href='#investments']").addClass('active');
		});
		
		$("#protection-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#protection']").tooltip('toggle');
			$("a[href='#protection']").addClass('active');
		});
		
		$("#retirement-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#retirement']").tooltip('toggle');
			$("a[href='#retirement']").addClass('active');
		});
					
		$(".servicesLink").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass("active");
			$(this).addClass("active");
			$(this).tooltip('toggle');
		});
				
					
		$("a[href='#consultancy']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#consultancy-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-0']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #consultancy-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #consultancy-content').css({display: 'block'});
		});
		
		$("a[href='#equity']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#equity-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-1']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #equity-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #equity-content').css({display: 'block'});
		});
		
		$("a[href='#inheritance']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#inheritance-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-2']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #inheritance-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #inheritance-content').css({display: 'block'});
		});
		
		$("a[href='#longTerm']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#longTerm-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-3']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #longTerm-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #longTerm-content').css({display: 'block'});
		});
		
		$("a[href='#mortgages']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#mortgages-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-4']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #mortgages-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #mortgages-content').css({display: 'block'});
		});
		
		
		$("a[href='#pensions']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#pensions-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-5']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #pensions-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #pensions-content').css({display: 'block'});
		});
		
		$("a[href='#investments']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#investments-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-6']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #investments-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #investments-content').css({display: 'block'});
		});
		
		$("a[href='#protection']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#protection-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-7']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #protection-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #protection-content').css({display: 'block'});
		});
		
		$("a[href='#retirement']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#retirement-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-8']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #retirement-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #retirement-content').css({display: 'block'});
		});
				
		$("a[href='#backtotop']").click(function(){
			$("html, body").animate({scrollTop: 0}, "slow");
			return false;
		});
		
	
