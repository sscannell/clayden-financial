				
		$('.servicesBanner a').tooltip({trigger: 'manual'})
				
		$(".servicesLink").hover(function() {
			$(this).tooltip('show');
						
		},function() {
			$(this).tooltip('hide');

		});
		
						
		$(".servicesLink").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$(this).tooltip('toggle');
			$(this).addClass('active');
		});
		
		$("#consultancy-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#consultancy']").tooltip('toggle');
			$("a[href='#consultancy']").addClass('active');
		});
								
		$("#pensions-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#pensions']").tooltip('toggle');
			$("a[href='#pensions']").addClass('active');
		});
		
		$("#investments-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#investments']").tooltip('toggle');
			$("a[href='#investments']").addClass('active');
		});
				
		$("#insurance-tab").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#insurance']").tooltip('toggle');
			$("a[href='#insurance']").addClass('active');
		});
					
		$(".servicesLink").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass("active");
			$(this).addClass("active");
			$(this).tooltip('toggle');
		});
				
					
		$("a[href='#consultancy']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#consultancy-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-3']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #consultancy-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #consultancy-content').css({display: 'block'});
		});
								
		
		$("a[href='#pensions']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#pensions-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-0']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #pensions-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #pensions-content').css({display: 'block'});
		});
		
		$("a[href='#investments']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#investments-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-2']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #investments-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #investments-content').css({display: 'block'});
		});
				
		$("a[href='#insurance']").click(function(){
			$('.resp-tab-item').removeClass('resp-tab-active');
			$('#insurance-tab').addClass('resp-tab-active');
			$('.resp-accordion').removeClass('resp-tab-active');
			$("h2[aria-controls='tab_item-1']").addClass('resp-tab-active');
			$('.resp-tab-content').removeClass('resp-tab-content-active');
			$('.resp-tab-content').css({display: 'none'});
			$('.resp-tabs-container #insurance-content').addClass('resp-tab-content-active');
			$('.resp-tabs-container #insurance-content').css({display: 'block'});
		});
		
		$("a[href='#backtotop']").click(function(){
			$("html, body").animate({scrollTop: 0}, "slow");
			return false;
		});
		
		$("h2[aria-controls='tab_item-0']").click(function() {
			$(".servicesLink").tooltip('hide');
			$(".servicesLink").removeClass('active');
			$("a[href='#pensions']").tooltip('toggle');
			$("a[href='#pensions']").addClass('active');
		});
		
	
